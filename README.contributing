* Useful Makefile targets

The following 'make' targets exist for your convenience:

  make or make all
        build all languages in all available formats

  make html
        test build all languages with html

  make text
        test build all languages with plain text

  make pdf
        test build all valid languages with pdf

  make nopdf
        test build all non-valid languages with pdf

  make epub
        test build all languages with epub

  make info
        test build all languages with info

  make LANGS=en html
        build the English manual in HTML format

  make LANGS=fr pdf
        build the French manual in PDF format

  make LANGS_NOPDF=ja pdf
        build the Japanese place holder manual in PDF format

  make update-po
        update .po files for all languages

  DESTDIR=tmp sudo -E make install
        build all languages in all available formats
        and install under ./tmp

* Contacting

To contain the maintainers of this package, email
<developers-reference@packages.debian.org>.


* Contributing

If you want to contribute to the Developer's Reference, it's best to
first submit a few patches as bug reports.  Writing patches for
existing bugs are also always appreciated.  You may wish to make
patches against the Git sources, about which see below.

When making changes, please try to use short lines of text (< 64
characters). This makes it easier to use 'diff' to see the
differences. (See discussion in <https://bugs.debian.org/278267>)

Do not commit patches to the developers reference yourself unless
authorized to do so. Patches need to be finalized and common opinion
before they are applied.


* Writing style

Please use gender-neutral formulations. This means avoiding
pronouns like he/she when referring to a role (like "maintainer")
whose gender is unknown. Instead of you should use the
plural form (singular they [1]).

[1] https://en.wikipedia.org/wiki/Singular_they


* Sphinx style

Section header underlines are intentionally set much longer than needed
for English source to accommodate translations which may yield longer
strings.  Please make sure to keep the length of Section header text
below 100 characters for all languages.

Don't use ASCII character only TeX-style writing like `quoted-word' or
``double-quoted-word''.  These interfere with Sphinx markups.

Keep markups minimal and consistent.

* Git

This manual is a part of the Debian Documentation Project (DDP)
and its source is managed in a Git repository in the Debian
group on Salsa, see
<https://wiki.debian.org/Salsa/Doc#Collaborative_Maintenance:_.22Debian.22_group>.

If you just want to check out the current Git version of this
manual to create patches against, you can use the command to do so:

git clone https://salsa.debian.org/debian/developers-reference.git

For write access (automaticly given to all Debian Developers):

git clone git@salsa.debian.org:debian/developers-reference.git


* Translators

We have tried to keep language-independent bits of text with rst_epilog
variable defined in conf.py.  Feel free to truck stuff out of the
English manual into rst_epilog if it's useful, or else report the
problem.

The translation PO files are in source/locales/**/LC_MESSAGES/*.po, we
hope very much for more translations.


# vim: set sw=2 sts=2 et ai si tw=72 :
